using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoWhales.Core.Domain;

namespace TwoWhales.Core.Repositories
{
    public class ToursRepository : IDisposable
    {
        private ModelsContainer modelsContainer;

        private bool disposed;

        public ToursRepository()
        {
            modelsContainer = new ModelsContainer();
            disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    modelsContainer.Dispose();
                }

                disposed = true;
            }
        }

        ~ToursRepository()
        {
            Dispose(false);
        }

        public List<Tour> GetAllTours()
        {
            return modelsContainer.Tours
                .ToList();
        }

        public Tour GetTour(int id)
        {
            return GetAllTours()
                .FirstOrDefault(tour => tour.Id == id);
        }

        public void SaveTour(Tour tour)
        {
            if (tour.Id != -1)
                modelsContainer.Tours.Attach(tour);
            else
                modelsContainer.Tours.Add(tour);

            modelsContainer.SaveChanges();
        }

        public void DeleteTour(int id)
        {
            modelsContainer.Tours.Remove(GetTour(id));

            modelsContainer.SaveChanges();
        }
    }
}
