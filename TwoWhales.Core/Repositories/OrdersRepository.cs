using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoWhales.Core.Domain;

namespace TwoWhales.Core.Repositories
{
    public class OrdersRepository : IDisposable
    {
        private ModelsContainer modelsContainer;

        private bool disposed;

        public OrdersRepository()
        {
            modelsContainer = new ModelsContainer();
            disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    modelsContainer.Dispose();
                }

                disposed = true;
            }
        }

        ~OrdersRepository()
        {
            Dispose(false);
        }

        public List<Order> GetAllOrders()
        {
            return modelsContainer.Orders
                .ToList();
        }

        public Order GetOrder(int id)
        {
            return GetAllOrders()
                .FirstOrDefault(order => order.Id == id);
        }

        public void SaveOrder(Order order)
        {
            if (order.Id != -1)
                modelsContainer.Orders.Attach(order);
            else
                modelsContainer.Orders.Add(order);

            modelsContainer.SaveChanges();
        }

        public void DeleteOrder(int id)
        {
            modelsContainer.Orders.Remove(GetOrder(id));

            modelsContainer.SaveChanges();
        }
    }
}
