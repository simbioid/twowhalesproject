﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoWhales.Core.Domain;

namespace TwoWhales.Core.Repositories
{
    public class EditorsRepository : IDisposable
    {
        private ModelsContainer modelsContainer;

        private bool disposed;

        public EditorsRepository()
        {
            modelsContainer = new ModelsContainer();
            disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    modelsContainer.Dispose();
                }

                disposed = true;
            }
        }

        ~EditorsRepository()
        {
            Dispose(false);
        }

        public List<Editor> GetAllEditors()
        {
            return modelsContainer.Users.OfType<Editor>()
                .Except((new AdministratorsRepository()).GetAllAdministrators())
                .ToList();
        }

        public Editor GetEditor(int id)
        {
            return GetAllEditors()
                .FirstOrDefault(editor => editor.Id == id);
        }

        public void SaveEditor(Editor editor)
        {
            if (editor.Id != -1)
                modelsContainer.Users.Attach(editor);
            else
                modelsContainer.Users.Add(editor);

            modelsContainer.SaveChanges();
        }

        public void DeleteEditor(int id)
        {
            modelsContainer.Users.Remove(GetEditor(id));

            modelsContainer.SaveChanges();
        }
    }
}
