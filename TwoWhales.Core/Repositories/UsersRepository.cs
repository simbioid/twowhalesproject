﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoWhales.Core.Domain;

namespace TwoWhales.Core.Repositories
{
    public class UsersRepository : IDisposable
    {
        private ModelsContainer modelsContainer;

        private bool disposed;

        public UsersRepository()
        {
            modelsContainer = new ModelsContainer();
            disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    modelsContainer.Dispose();
                }

                disposed = true;
            }
        }

        ~UsersRepository()
        {
            Dispose(false);
        }

        public List<User> GetAllUsers()
        {
            return modelsContainer.Users
                .Except((new EditorsRepository()).GetAllEditors())
                .ToList();
        }

        public User GetUser(int id)
        {
            return GetAllUsers()
                .FirstOrDefault(user => user.Id == id);
        }

        public void SaveUser(User user)
        {
            if (user.Id != -1)
                modelsContainer.Users.Attach(user);
            else
                modelsContainer.Users.Add(user);

            modelsContainer.SaveChanges();
        }

        public void DeleteUser(int id)
        {
            modelsContainer.Users.Remove(GetUser(id));

            modelsContainer.SaveChanges();
        }
    }
}
