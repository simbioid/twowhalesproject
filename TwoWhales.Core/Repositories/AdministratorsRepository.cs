﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoWhales.Core.Domain;

namespace TwoWhales.Core.Repositories
{
    public class AdministratorsRepository : IDisposable
    {
        private ModelsContainer modelsContainer;

        private bool disposed;

        public AdministratorsRepository()
        {
            modelsContainer = new ModelsContainer();
            disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    modelsContainer.Dispose();
                }

                disposed = true;
            }
        }

        ~AdministratorsRepository()
        {
            Dispose(false);
        }

        public List<Administrator> GetAllAdministrators()
        {
            return modelsContainer.Users.OfType<Administrator>()
                .ToList();
        }

        public Administrator GetAdministrator(int id)
        {
            return GetAllAdministrators()
                .FirstOrDefault(administrator => administrator.Id == id);
        }

        public void SaveAdministrator(Administrator administrator)
        {
            if (administrator.Id != -1)
                modelsContainer.Users.Attach(administrator);
            else
                modelsContainer.Users.Add(administrator);

            modelsContainer.SaveChanges();
        }

        public void DeleteAdministrator(int id)
        {
            modelsContainer.Users.Remove(GetAdministrator(id));

            modelsContainer.SaveChanges();
        }
    }
}
