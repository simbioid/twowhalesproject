﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoWhales.Core.Domain;
using TwoWhales.Core.Repositories;

namespace TwoWhales.Core.Managers
{
    public class AdministratorsManager : IDisposable
    {
        private AdministratorsRepository administratorsRepository;

        private bool disposed;

        public AdministratorsManager()
        {
            administratorsRepository = new AdministratorsRepository();
            disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    administratorsRepository.Dispose();
                }

                disposed = true;
            }
        }

        ~AdministratorsManager()
        {
            Dispose(false);
        }

        public List<Administrator> GetAllAdministrators()
        {
            return administratorsRepository.GetAllAdministrators();
        }

        public Administrator GetAdministrator(int id)
        {
            return administratorsRepository.GetAdministrator(id);
        }

        public void AddAdministrator(Administrator administrator)
        {
            administrator.Id = -1;

            administratorsRepository.SaveAdministrator(administrator);
        }

        public void UpdateAdministrator(Administrator administrator)
        {
            administratorsRepository.SaveAdministrator(administrator);
        }

        public void DeleteAdministrator(int id)
        {
            administratorsRepository.DeleteAdministrator(id);
        }
    }
}
