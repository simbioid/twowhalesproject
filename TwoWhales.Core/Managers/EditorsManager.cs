﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoWhales.Core.Domain;
using TwoWhales.Core.Repositories;

namespace TwoWhales.Core.Managers
{
    public class EditorsManager : IDisposable
    {
        private EditorsRepository editorsRepository;

        private bool disposed;

        public EditorsManager()
        {
            editorsRepository = new EditorsRepository();
            disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    editorsRepository.Dispose();
                }

                disposed = true;
            }
        }

        ~EditorsManager()
        {
            Dispose(false);
        }

        public List<Editor> GetAllEditors()
        {
            return editorsRepository.GetAllEditors();
        }

        public Editor GetEditor(int id)
        {
            return editorsRepository.GetEditor(id);
        }

        public void AddEditor(Editor editor)
        {
            editor.Id = -1;

            editorsRepository.SaveEditor(editor);
        }

        public void UpdateEditor(Editor editor)
        {
            editorsRepository.SaveEditor(editor);
        }

        public void DeleteEditor(int id)
        {
            editorsRepository.DeleteEditor(id);
        }
    }
}
