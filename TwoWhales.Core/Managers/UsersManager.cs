﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoWhales.Core.Domain;
using TwoWhales.Core.Repositories;

namespace TwoWhales.Core.Managers
{
    public class UsersManager : IDisposable
    {
        private UsersRepository usersRepository;

        private bool disposed;

        public UsersManager()
        {
            usersRepository = new UsersRepository();
            disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    usersRepository.Dispose();
                }

                disposed = true;
            }
        }

        ~UsersManager()
        {
            Dispose(false);
        }

        public List<User> GetAllUsers()
        {
            return usersRepository.GetAllUsers();
        }

        public User GetUser(int id)
        {
            return usersRepository.GetUser(id);
        }

        public void AddUser(User user)
        {
            user.Id = -1;

            usersRepository.SaveUser(user);
        }

        public void UpdateUser(User user)
        {
            usersRepository.SaveUser(user);
        }

        public void DeleteUser(int id)
        {
            usersRepository.DeleteUser(id);
        }
    }
}
