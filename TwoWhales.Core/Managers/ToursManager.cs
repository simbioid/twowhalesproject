using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoWhales.Core.Domain;
using TwoWhales.Core.Repositories;

namespace TwoWhales.Core.Managers
{
    public class ToursManager : IDisposable
    {
        private ToursRepository toursRepository;

        private bool disposed;

        public ToursManager()
        {
            toursRepository = new ToursRepository();
            disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    toursRepository.Dispose();
                }

                disposed = true;
            }
        }

        ~ToursManager()
        {
            Dispose(false);
        }

        public List<Tour> GetAllTours()
        {
            return toursRepository.GetAllTours();
        }

        public Tour GetTour(int id)
        {
            return toursRepository.GetTour(id);
        }

        public void AddTour(Tour tour)
        {
            tour.Id = -1;

            toursRepository.SaveTour(tour);
        }

        public void UpdateTour(Tour tour)
        {
            toursRepository.SaveTour(tour);
        }

        public void DeleteTour(int id)
        {
            toursRepository.DeleteTour(id);
        }
    }
}
