using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoWhales.Core.Domain;
using TwoWhales.Core.Repositories;

namespace TwoWhales.Core.Managers
{
    public class OrdersManager : IDisposable
    {
        private OrdersRepository ordersRepository;

        private bool disposed;

        public OrdersManager()
        {
            ordersRepository = new OrdersRepository();
            disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    ordersRepository.Dispose();
                }

                disposed = true;
            }
        }

        ~OrdersManager()
        {
            Dispose(false);
        }

        public List<Order> GetAllOrders()
        {
            return ordersRepository.GetAllOrders();
        }

        public Order GetOrder(int id)
        {
            return ordersRepository.GetOrder(id);
        }

        public void AddOrder(Order order)
        {
            order.Id = -1;

            ordersRepository.SaveOrder(order);
        }

        public void UpdateOrder(Order order)
        {
            ordersRepository.SaveOrder(order);
        }

        public void DeleteOrder(int id)
        {
            ordersRepository.DeleteOrder(id);
        }
    }
}
