
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/15/2014 20:44:25
-- Generated from EDMX file: C:\Users\Alex Sid\Documents\Visual Studio 2013\Projects\TwoWhales\TwoWhales.Core\Domain\DatabaseModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Database];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Administrator_inherits_Editor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users_Administrator] DROP CONSTRAINT [FK_Administrator_inherits_Editor];
GO
IF OBJECT_ID(N'[dbo].[FK_Editor_inherits_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users_Editor] DROP CONSTRAINT [FK_Editor_inherits_User];
GO
IF OBJECT_ID(N'[dbo].[FK_TourOrder]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_TourOrder];
GO
IF OBJECT_ID(N'[dbo].[FK_UserOrder]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_UserOrder];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Orders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Orders];
GO
IF OBJECT_ID(N'[dbo].[Tours]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tours];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Users_Administrator]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users_Administrator];
GO
IF OBJECT_ID(N'[dbo].[Users_Editor]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users_Editor];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [Login] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Phone] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Orders'
CREATE TABLE [dbo].[Orders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] int  NOT NULL,
    [TourId] int  NOT NULL
);
GO

-- Creating table 'Tours'
CREATE TABLE [dbo].[Tours] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Country] nvarchar(max)  NOT NULL,
    [City] nvarchar(max)  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [FinishDate] datetime  NOT NULL,
    [Transport] nvarchar(max)  NOT NULL,
    [FullCost] float  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Users_Editor'
CREATE TABLE [dbo].[Users_Editor] (
    [Id] int  NOT NULL
);
GO

-- Creating table 'Users_Administrator'
CREATE TABLE [dbo].[Users_Administrator] (
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [UserId], [TourId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [PK_Orders]
    PRIMARY KEY CLUSTERED ([UserId], [TourId] ASC);
GO

-- Creating primary key on [Id] in table 'Tours'
ALTER TABLE [dbo].[Tours]
ADD CONSTRAINT [PK_Tours]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users_Editor'
ALTER TABLE [dbo].[Users_Editor]
ADD CONSTRAINT [PK_Users_Editor]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users_Administrator'
ALTER TABLE [dbo].[Users_Administrator]
ADD CONSTRAINT [PK_Users_Administrator]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_UserOrder]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TourId] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_TourOrder]
    FOREIGN KEY ([TourId])
    REFERENCES [dbo].[Tours]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TourOrder'
CREATE INDEX [IX_FK_TourOrder]
ON [dbo].[Orders]
    ([TourId]);
GO

-- Creating foreign key on [Id] in table 'Users_Editor'
ALTER TABLE [dbo].[Users_Editor]
ADD CONSTRAINT [FK_Editor_inherits_User]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Users_Administrator'
ALTER TABLE [dbo].[Users_Administrator]
ADD CONSTRAINT [FK_Administrator_inherits_Editor]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Users_Editor]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------