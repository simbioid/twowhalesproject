﻿<%@ Page Language="C#" StylesheetTheme="DefaultTheme" Title="Два Кита" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TwoWhales.Web.Default" MasterPageFile="~/Site.Master" %>

<asp:Content ContentPlaceHolderID="HeadContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <div id="main_interface">
        <asp:Panel runat="server">
            <span id="welcome_label">
                <asp:Label runat="server" Text="Добро пожаловать!" />
            </span>
            <ul>
                <li><a href="Default.aspx">Редактировать личные данные</a></li>
                <li><a href="Default.aspx">Приступить к бронированию поездок</a></li>
                <li><a href="Default.aspx">Редактировать информацию о поездках</a></li>
                <li><a href="Default.aspx">Редактировать информацию о пользователях</a></li>
            </ul>
        </asp:Panel>
    </div>

    <div id="authentification_menu">
        <asp:Panel runat="server">
            <div id="aaa">
                <asp:Label runat="server" Text="Логин" />
                <br />
                <asp:TextBox runat="server" />
                <br />
                <asp:Label runat="server" Text="Пароль" />
                <br />
                <asp:TextBox runat="server" TextMode="Password" />
                <br />
                <asp:CheckBox runat="server" Text="Запомнить меня" />
                <br />
                <asp:LinkButton runat="server" Text="Войти" />
                <br />
                <asp:LinkButton runat="server" Text="Зарегистрироваться" />
            </div>
        </asp:Panel>
    </div>

    <div id="tours">
        <asp:Panel runat="server">
            <asp:GridView runat="server" Width="100%" Height="100%" ShowHeader="False">

            </asp:GridView>
        </asp:Panel> 
    </div>
</asp:Content>